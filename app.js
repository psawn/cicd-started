const express = require("express");
const redis = require("redis");
const Pool = require("pg").Pool;
const AWS = require("aws-sdk");
const fs = require("fs");
const multer = require("multer");
const bodyParser = require("body-parser");

const app = express();
const port = 3001;
const upload = multer();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require("dotenv").config();

const pool = new Pool({
  user: process.env.USER,
  host: process.env.HOST,
  database: process.env.DATABASE,
  password: process.env.PASSWORD,
  port: process.env.PORT,
});

const client = redis.createClient({
  socket: {
    host: "localhost",
    port: 6379,
  },
  password: "SUPER_SECRET_PASSWORD",
});

const s3 = new AWS.S3({
  credentials: {
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
  },
});

const connectRedis = async () => {
  await client.connect();
};

connectRedis();

client.set("key", "value");

app.get("/user", (req, res) => {
  pool.query("select * from Persons", (error, results) => {
    if (error) {
      res.status(500).json(error);
    }
    res.status(200).json(results);
  });
});

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/redis", async (req, res) => {
  const data = await client.get("key");
  res.status(200).json({ data });
});

app.post("/single-upload-s3", upload.single("file"), async (req, res) => {
  console.l;
  const data = await s3
    .upload({
      Bucket: process.env.BUCKET,
      Key: `images/${req.file.originalname}`,
      Body: req.file.buffer,
    })
    .promise();
  res.status(200).json({ data });
});

app.post("/get-object-url-s3", async (req, res) => {
  const fileName = req.body.fileName;
  const data = s3.getSignedUrl("getObject", {
    Bucket: process.env.BUCKET,
    Key: `images/${fileName}`,
    Expires: 900,
  });
  res.status(200).json({ data });
});

app.post("/presigned-upload-s3", async (req, res) => {
  const fileName = req.body.fileName;
  const data = s3.getSignedUrl("putObject", {
    Bucket: process.env.BUCKET,
    // Key: `images/${fileName}`,
    Key: fileName,
    Expires: 900,
    ContentType: "image/png",
  });
  res.status(200).json({ data });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
